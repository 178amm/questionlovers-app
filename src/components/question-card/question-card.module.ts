import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { QuestionCardComponent } from './question-card';

@NgModule({
  declarations: [
    QuestionCardComponent,
  ],
  imports: [
    IonicPageModule.forChild(QuestionCardComponent),
  ],
  exports: [
    QuestionCardComponent
  ]
})
export class QuestionCardComponentModule {}
