import { Component, Input } from '@angular/core';

import { Question } from '../../models/question';

/**
 * Generated class for the QuestionCardComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'question-card',
  templateUrl: 'question-card.html'
})
export class QuestionCardComponent {

	@Input() question: Question;
  	text: string;

	constructor() {
	    console.log('Hello QuestionCardComponent Component');
	    this.text = 'Al contrario del pensamiento popular, el texto de Lorem Ipsum no es simplemente texto aleatorio. Richard McClintock, un profesor de Latin de la Universidad de Hampden-Sydney en Virginia, encontró una de las palabras más oscuras de la lengua del latín';
	}
}
