import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MatchingBarComponent } from './matching-bar';

@NgModule({
  declarations: [
    MatchingBarComponent,
  ],
  imports: [
    IonicPageModule.forChild(MatchingBarComponent),
  ],
  exports: [
    MatchingBarComponent
  ]
})
export class MatchingBarComponentModule {}
