import { Component } from '@angular/core';

/**
 * Generated class for the MatchingBarComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'matching-bar',
  templateUrl: 'matching-bar.html'
})
export class MatchingBarComponent {

  text: string;

  constructor() {
    console.log('Hello MatchingBarComponent Component');
    this.text = 'Hello World';
  }

}
