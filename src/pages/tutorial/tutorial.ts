import { Component } from '@angular/core';

import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { Storage } from '@ionic/storage';

import { QuestionCardComponent } from '../../components/question-card/question-card';
import { MatchingBarComponent } from '../../components/matching-bar/matching-bar';
import { Question } from '../../models/question';

/**
 * Generated class for the TutorialPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */
@IonicPage()
@Component({
  selector: 'page-tutorial',
  templateUrl: 'tutorial.html',
})
export class TutorialPage {

	questions: Question[];

  constructor(public navCtrl: NavController, public navParams: NavParams, private storage: Storage) {
  	//Init cards
  	this.questions = [
  	new Question(
  		{
  			_id: '8982398239829',
  			by: 'dicoto',
  			text: 'Bienvenido a mi maravillosa aplicacion',
  			positive: 'Me encanta',
  			negative: 'Menuda mierda'
  		}), 
  	new Question(
  		{
  			id: '8982398239829',
  			by: 'dicoto',
  			text: 'Tienes ganas de mas?',
  			positive: 'siiiii',
  			negative: 'Me aburro'
  		})]
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TutorialPage');
  }

}
